/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Command} from 'commander';
import {existsSync, readFileSync, writeFileSync} from 'fs';
import path from 'path';
import {cwd} from 'process';
import {
  checkCIConfig,
  checkConfigurationFilesAreExtended,
  checkContributors,
  checkCopyrightYears,
  checkDependencies,
  checkLicenses,
  checkNeededFiles,
  checkNYCConfiguration,
  checkScripts,
  consoleInfo,
  consoleLog,
  getConfiguration,
  getRules,
} from './common';

// current working directory
const currentWorkingDirectory = cwd();

// instantiate new commander
const commander = new Command('openstapps-configuration');

// configure commander
// eslint-disable-next-line unicorn/prefer-module
commander.version(JSON.parse(readFileSync(path.resolve(__dirname, '..', 'package.json')).toString()).version);

commander
  .option(
    '-p, --path <path>',
    `Path of project to add files to (${currentWorkingDirectory})`,
    currentWorkingDirectory,
  )
  .option('-r, --replace', 'Whether to replace existing files or not', false)
  .parse(process.argv);

// make path absolute
const projectPath = path.resolve(commander.opts().path);

// check for existing package.json in provided path
if (!existsSync(path.resolve(projectPath, 'package.json'))) {
  throw new Error(`No 'package.json' in '${projectPath}'.`);
}

// path to examined package.json
const packageJsonPath = path.resolve(projectPath, 'package.json');

// read package.json in provided path
const packageJson = JSON.parse(readFileSync(packageJsonPath).toString());

// check if provided path is this package
if (packageJson.name === '@openstapps/configuration') {
  consoleInfo("I'm not going to check myself!");
  process.exit(0);
}

// whether the contents of the package.json were changed
let packageJsonChanged = false;

// whether to suggest an overwrite
let suggestOverwrite = false;

const configuration = getConfiguration(packageJson);

const rules = getRules(configuration);

checkDependencies(rules, packageJson);

checkLicenses(rules, packageJson);

checkConfigurationFilesAreExtended(projectPath);

suggestOverwrite = suggestOverwrite || checkNeededFiles(rules, projectPath, commander.opts().replace);

const checkedNYCConfiguration = checkNYCConfiguration(rules, packageJson, commander.opts().replace);

packageJsonChanged = packageJsonChanged || checkedNYCConfiguration[0];
suggestOverwrite = suggestOverwrite || checkedNYCConfiguration[1];

packageJsonChanged = packageJsonChanged || checkScripts(rules, packageJson, commander.opts().replace);

checkContributors(projectPath, packageJson);

checkCIConfig(rules, projectPath);

checkCopyrightYears(projectPath, 'src', commander.opts().replace);

const indentation = 2;

if (packageJsonChanged) {
  writeFileSync(
    path.resolve(projectPath, 'package.json'),
    // eslint-disable-next-line unicorn/no-null
    `${JSON.stringify(packageJson, null, indentation)}\n`,
  );
  consoleLog(`Changes were written to '${packageJsonPath}'.`);
}

if (suggestOverwrite) {
  consoleInfo(`You should consider to overwrite your configuration files and check for intended derivations:
npm run check-configuration -- -r`);
}

consoleLog(`Done checking the configuration in '${projectPath}'.`);
