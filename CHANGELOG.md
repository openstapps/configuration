# [0.34.0](https://gitlab.com/openstapps/configuration/compare/v0.33.0...v0.34.0) (2023-01-17)



# [0.33.0](https://gitlab.com/openstapps/configuration/compare/v0.32.2...v0.33.0) (2022-08-17)


### Features

* add proper coverage to ci templates ([35b4b6c](https://gitlab.com/openstapps/configuration/commit/35b4b6c8237a167518f0edb7326578095c935764))



## [0.32.2](https://gitlab.com/openstapps/configuration/compare/v0.32.1...v0.32.2) (2022-07-05)


### Bug Fixes

* recommended CI settings ([0753ab9](https://gitlab.com/openstapps/configuration/commit/0753ab9f33d13eb2cd7d6512934fd285906f2c47))



## [0.32.1](https://gitlab.com/openstapps/configuration/compare/v0.32.0...v0.32.1) (2022-07-05)


### Bug Fixes

* simplify dependency version checking ([df92e97](https://gitlab.com/openstapps/configuration/commit/df92e97cb65df3088b090ba76320d05938948514))



# [0.32.0](https://gitlab.com/openstapps/configuration/compare/v0.31.0...v0.32.0) (2022-06-27)


### Features

* copyright notice year replacement ([17a796e](https://gitlab.com/openstapps/configuration/commit/17a796e97b95694ee39dae0958c99279a1936d74))



# [0.31.0](https://gitlab.com/openstapps/configuration/compare/v0.30.0...v0.31.0) (2022-06-02)


### Bug Fixes

* npmignore eslint exceptions ([405f8cf](https://gitlab.com/openstapps/configuration/commit/405f8cf496aee4f8f44ef61ee2b61c4a98343114))



# [0.30.0](https://gitlab.com/openstapps/configuration/compare/v0.29.1...v0.30.0) (2022-06-01)


### Features

* transition from TSLint to ESLint ([9430e10](https://gitlab.com/openstapps/configuration/commit/9430e103874e2c9c41a8c29b13e1ae56d2379af3))



## [0.29.1](https://gitlab.com/openstapps/configuration/compare/v0.29.0...v0.29.1) (2022-05-27)



# [0.29.0](https://gitlab.com/openstapps/configuration/compare/v0.28.1...v0.29.0) (2021-12-14)



## [0.28.1](https://gitlab.com/openstapps/configuration/compare/v0.28.0...v0.28.1) (2021-09-13)


### Bug Fixes

* adjust command of typedoc (upgrade typescript) ([7d396f9](https://gitlab.com/openstapps/configuration/commit/7d396f92026ae84888e1b2246f11d3995a4bf11f))



# [0.28.0](https://gitlab.com/openstapps/configuration/compare/v0.27.0...v0.28.0) (2021-09-13)



# [0.27.0](https://gitlab.com/openstapps/configuration/compare/v0.26.0...v0.27.0) (2021-04-06)



# [0.26.0](https://gitlab.com/openstapps/configuration/compare/v0.25.0...v0.26.0) (2021-02-22)



# [0.25.0](https://gitlab.com/openstapps/configuration/compare/v0.24.0...v0.25.0) (2020-09-17)


### Bug Fixes

* exclude test dir and its subdirs for tslint ([ec15ebe](https://gitlab.com/openstapps/configuration/commit/ec15ebe5c5fb30e638cc721ca916c84186b1d1e0)), closes [#34](https://gitlab.com/openstapps/configuration/issues/34)


### Features

* Change 'npm audit' failure behaviour ([c11b1da](https://gitlab.com/openstapps/configuration/commit/c11b1da9a6546e006bea29cc7f524b94736f7bfb))



# [0.24.0](https://gitlab.com/openstapps/configuration/compare/v0.23.0...v0.24.0) (2020-03-03)



# [0.23.0](https://gitlab.com/openstapps/configuration/compare/v0.22.0...v0.23.0) (2020-02-10)



# [0.22.0](https://gitlab.com/openstapps/configuration/compare/v0.21.1...v0.22.0) (2019-11-25)



## [0.21.1](https://gitlab.com/openstapps/configuration/compare/v0.21.0...v0.21.1) (2019-07-15)


### Bug Fixes

* remove package-job when forPackaging is false ([b8cefa4](https://gitlab.com/openstapps/configuration/commit/b8cefa42543b0c370b9b23ca7d1a60b45907771c))



# [0.21.0](https://gitlab.com/openstapps/configuration/compare/v0.20.0...v0.21.0) (2019-06-24)


### Bug Fixes

* update the nyc configuration ([8ece33b](https://gitlab.com/openstapps/configuration/commit/8ece33b6e6902040d42e8eb0b18237065697673e))



# [0.20.0](https://gitlab.com/openstapps/configuration/compare/v0.19.0...v0.20.0) (2019-06-06)


### Bug Fixes

* only check copyright years for TypeScript files ([1a4ec52](https://gitlab.com/openstapps/configuration/commit/1a4ec52712a2bc6b58c766141816110292f433ef)), closes [#25](https://gitlab.com/openstapps/configuration/issues/25)
* skip template lines to avoid parser errors ([b995bb5](https://gitlab.com/openstapps/configuration/commit/b995bb5c1439bb2860c04d18cfc485eda7ad110f)), closes [#26](https://gitlab.com/openstapps/configuration/issues/26)



# [0.19.0](https://gitlab.com/openstapps/configuration/compare/v0.18.0...v0.19.0) (2019-06-04)



# [0.18.0](https://gitlab.com/openstapps/configuration/compare/v0.17.2...v0.18.0) (2019-05-29)


### Features

* automatically generate changelog after version bump ([cbee9b8](https://gitlab.com/openstapps/configuration/commit/cbee9b888cccd2192e6725c7f36e3826becae95b)), closes [#23](https://gitlab.com/openstapps/configuration/issues/23)



## [0.17.2](https://gitlab.com/openstapps/configuration/compare/v0.17.1...v0.17.2) (2019-05-28)


### Features

* exclude tests from linting ([9e7f467](https://gitlab.com/openstapps/configuration/commit/9e7f46707026239875848048ddffdad3cf4f8c0d))



## [0.17.1](https://gitlab.com/openstapps/configuration/compare/v0.17.0...v0.17.1) (2019-05-28)


### Bug Fixes

* fix regression with copyright years ([5659295](https://gitlab.com/openstapps/configuration/commit/5659295b883034296d84aaefc3b00072797a63f9))



# [0.17.0](https://gitlab.com/openstapps/configuration/compare/v0.16.1...v0.17.0) (2019-05-28)


### Features

* add rule to enforce completed docs ([527a6f9](https://gitlab.com/openstapps/configuration/commit/527a6f9364113405786ffb7e6533842e4d8898ac)), closes [#20](https://gitlab.com/openstapps/configuration/issues/20)
* add sctricter TSLint rules ([76ebbfa](https://gitlab.com/openstapps/configuration/commit/76ebbfab5376c9954c7e9d4e0d9c715089ef1c8b)), closes [#21](https://gitlab.com/openstapps/configuration/issues/21)
* adjust TSLint command to enable advanced rules ([14b6420](https://gitlab.com/openstapps/configuration/commit/14b6420c33aec90d55023aa9b64fd0e69758fffa)), closes [#21](https://gitlab.com/openstapps/configuration/issues/21)



## [0.16.1](https://gitlab.com/openstapps/configuration/compare/v0.16.0...v0.16.1) (2019-05-24)


### Bug Fixes

* make sure that npmrc is created in correct home directory ([60f7e4c](https://gitlab.com/openstapps/configuration/commit/60f7e4cae28775b67b48aae5624a8062951c5264))



# [0.16.0](https://gitlab.com/openstapps/configuration/compare/v0.15.0...v0.16.0) (2019-05-22)


### Bug Fixes

* use lib as artifacts path ([ec8c891](https://gitlab.com/openstapps/configuration/commit/ec8c891a7a41c5308ad8a3d0acf8ff5c17754a93)), closes [#19](https://gitlab.com/openstapps/configuration/issues/19)



# [0.15.0](https://gitlab.com/openstapps/configuration/compare/v0.14.0...v0.15.0) (2019-05-20)


### Bug Fixes

* remove incremental flag ([73e6f24](https://gitlab.com/openstapps/configuration/commit/73e6f2413b462ddcea6b30e432822a2431384647)), closes [#15](https://gitlab.com/openstapps/configuration/issues/15)
* remove wrong excluded path from nyc config ([cf81c1e](https://gitlab.com/openstapps/configuration/commit/cf81c1e5201213dd9302ecf302b372073f1c480e)), closes [#16](https://gitlab.com/openstapps/configuration/issues/16)


### Features

* add check for copyright years ([44c82ad](https://gitlab.com/openstapps/configuration/commit/44c82ade69938157f6b8dfc9a709d33ac73c8da5)), closes [#17](https://gitlab.com/openstapps/configuration/issues/17)
* add rules to enforce automatic package publishing ([1e3e7df](https://gitlab.com/openstapps/configuration/commit/1e3e7dfac8c0887bc3c5758c35fce5ad9b4ebef3)), closes [#18](https://gitlab.com/openstapps/configuration/issues/18)
* warn about extraneous copyright years ([1c826a4](https://gitlab.com/openstapps/configuration/commit/1c826a41e0738f2863809f56edd8be53fdfd6e35)), closes [#17](https://gitlab.com/openstapps/configuration/issues/17)



# [0.14.0](https://gitlab.com/openstapps/configuration/compare/v0.13.0...v0.14.0) (2019-04-30)


### Features

* add possibility to ignore CI entries ([116ddeb](https://gitlab.com/openstapps/configuration/commit/116ddebfb8714aca85612a55fc0ee398022eb0d3)), closes [#14](https://gitlab.com/openstapps/configuration/issues/14)



# [0.13.0](https://gitlab.com/openstapps/configuration/compare/v0.12.0...v0.13.0) (2019-04-18)


### Features

* add configuration for configuration checks ([da2d7f6](https://gitlab.com/openstapps/configuration/commit/da2d7f6f91ccfc91d3560c1e380daa4282f1aeb5)), closes [#13](https://gitlab.com/openstapps/configuration/issues/13)



# [0.12.0](https://gitlab.com/openstapps/configuration/compare/v0.11.0...v0.12.0) (2019-04-16)


### Bug Fixes

* do not escape newlines in scripts ([77bed07](https://gitlab.com/openstapps/configuration/commit/77bed076bbf439a301b6ce4eb4a536ab2b000570))


### Features

* add expected build script ([34d3d43](https://gitlab.com/openstapps/configuration/commit/34d3d43ff5963bc2dae95cd8d6eb5def8023f7fc)), closes [#12](https://gitlab.com/openstapps/configuration/issues/12)



# [0.11.0](https://gitlab.com/openstapps/configuration/compare/v0.10.0...v0.11.0) (2019-04-15)


### Bug Fixes

* exclude docs from packages ([d7b6ecb](https://gitlab.com/openstapps/configuration/commit/d7b6ecb734c008fbad5a9615bf35665fdac20fac)), closes [#11](https://gitlab.com/openstapps/configuration/issues/11)



# [0.10.0](https://gitlab.com/openstapps/configuration/compare/v0.9.0...v0.10.0) (2019-04-09)


### Features

* add check for contributors ([ab81aab](https://gitlab.com/openstapps/configuration/commit/ab81aab046248e8b8379576914b58e23a03fec45))
* check entries in CI config ([61d2285](https://gitlab.com/openstapps/configuration/commit/61d2285a1fd76dee1c02fd2318a92cd2fbe4df71)), closes [#10](https://gitlab.com/openstapps/configuration/issues/10)



# [0.9.0](https://gitlab.com/openstapps/configuration/compare/v0.8.0...v0.9.0) (2019-04-08)



# [0.8.0](https://gitlab.com/openstapps/configuration/compare/v0.7.0...v0.8.0) (2019-04-01)


### Bug Fixes

* remove @types/chalk ([bf26eff](https://gitlab.com/openstapps/configuration/commit/bf26effe688299ba18047ca3a8c412ddd162b9c1))



# [0.7.0](https://gitlab.com/openstapps/configuration/compare/v0.6.0...v0.7.0) (2019-02-26)


### Features

* add ability to check scripts ([662f534](https://gitlab.com/openstapps/configuration/commit/662f53455097ba13c84e20111d99bf60c3f967c2)), closes [#6](https://gitlab.com/openstapps/configuration/issues/6)



# [0.6.0](https://gitlab.com/openstapps/configuration/compare/v0.5.1...v0.6.0) (2019-02-13)


### Bug Fixes

* correctly check tslint.json ([9c8ce48](https://gitlab.com/openstapps/configuration/commit/9c8ce486ce03ebff344b53b566afa3e164c60647)), closes [#3](https://gitlab.com/openstapps/configuration/issues/3) [#4](https://gitlab.com/openstapps/configuration/issues/4)



## [0.5.1](https://gitlab.com/openstapps/configuration/compare/v0.5.0...v0.5.1) (2019-01-28)


### Bug Fixes

* exclude CLI file in nyc configuration ([c46dc18](https://gitlab.com/openstapps/configuration/commit/c46dc1828c826c7025017314fdf2fdce70a58772)), closes [#5](https://gitlab.com/openstapps/configuration/issues/5)



# [0.5.0](https://gitlab.com/openstapps/configuration/compare/v0.4.0...v0.5.0) (2018-12-18)


### Bug Fixes

* use template for .gitignore ([04b6154](https://gitlab.com/openstapps/configuration/commit/04b6154f9a13e68db00a2d80f54a719482c9a4f9))



# [0.4.0](https://gitlab.com/openstapps/configuration/compare/v0.3.1...v0.4.0) (2018-12-11)


### Features

* disallow spaces in curly braces ([4e14415](https://gitlab.com/openstapps/configuration/commit/4e1441564b6785c118f14ac94659cf1339c0dac3)), closes [#2](https://gitlab.com/openstapps/configuration/issues/2)



## [0.3.1](https://gitlab.com/openstapps/configuration/compare/v0.3.0...v0.3.1) (2018-12-04)



# [0.3.0](https://gitlab.com/openstapps/configuration/compare/v0.2.0...v0.3.0) (2018-12-04)


### Features

* add cli to copy configurations ([98aa5a5](https://gitlab.com/openstapps/configuration/commit/98aa5a5f70bb17ca05574a291831158eea7920ef)), closes [#1](https://gitlab.com/openstapps/configuration/issues/1)
* add script to check configuration ([adfc5aa](https://gitlab.com/openstapps/configuration/commit/adfc5aa5dc149cb6d634015baceea5839ae29190))



# [0.2.0](https://gitlab.com/openstapps/configuration/compare/v0.1.1...v0.2.0) (2018-11-30)


### Features

* copy .editorconfig during installation ([f228af2](https://gitlab.com/openstapps/configuration/commit/f228af24f9a20457f89719c093cc9239f35cf0b3))



## [0.1.1](https://gitlab.com/openstapps/configuration/compare/v0.1.0...v0.1.1) (2018-11-30)



# [0.1.0](https://gitlab.com/openstapps/configuration/compare/v0.0.1...v0.1.0) (2018-11-30)


### Features

* add editor config ([ecbc768](https://gitlab.com/openstapps/configuration/commit/ecbc768b32daab5f32c5f91aeac9252937586465))



## [0.0.1](https://gitlab.com/openstapps/configuration/compare/e2bb08d1e879f8e2aac0f59930f50ff9dfe7526b...v0.0.1) (2018-11-28)


### Features

* add tslint and tsconfig ([e2bb08d](https://gitlab.com/openstapps/configuration/commit/e2bb08d1e879f8e2aac0f59930f50ff9dfe7526b))



