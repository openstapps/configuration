# @openstapps/configuration

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/configuration.svg?style=flat-square)](https://gitlab.com/openstapps/configuration/commits/master) 
[![npm](https://img.shields.io/npm/v/@openstapps/configuration.svg?style=flat-square)](https://npmjs.com/package/@openstapps/configuration)
[![license)](https://img.shields.io/npm/l/@openstapps/configuration.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/configuration)

Checks your `@openstapps` project's configuration and automatically adjusts it to adhere to the suggested defaults.

## Installation

Install it as a dev dependency in your project and it will check your configuration and add a script in your `package.json` to check the configuration again easily afterwards.

```shell
npm install --save-dev @openstapps/configuration
npm run check-configuration
```

You can also install it globally and use it as a cli.

```shell
npm install -g @openstapps/configuration
openstapps-configuration --help
```

## Configuration

To configure how your project's configuration is checked add a property `"openstappsConfiguration"` to your `package.json` with all or some properties of the [configuration](https://openstapps.gitlab.io/configuration/interfaces/_common_.configuration.html).
